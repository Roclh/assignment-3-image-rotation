#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H
#include "errorUtils.h"
#include "fileUtils.h"
#include "image.h"
#include <inttypes.h>
#include <stdio.h>

int fromBMP(FILE* to, struct image* img);

int toBMP(FILE* out, struct image img);

#endif //IMAGE_TRANSFORMER_BMP_H
