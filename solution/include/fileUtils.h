#ifndef IMAGE_TRANSFORMER_FILEUTILS_H
#define IMAGE_TRANSFORMER_FILEUTILS_H

#include "errorUtils.h"
#include <stdio.h>

int readF(const char *pathname, FILE **file);

int writeF(const char *pathname, FILE **file);

int closeF(FILE *file);

#endif //IMAGE_TRANSFORMER_FILEUTILS_H
