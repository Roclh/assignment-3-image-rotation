#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include <stdbool.h>
#include <stdint.h>
struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

extern const struct image wrongImage;

struct image initImage(uint64_t width, uint64_t height);

struct pixel* target(struct image img, uint64_t x, uint64_t y);

bool destroyImage(struct image *image);
#endif //IMAGE_TRANSFORMER_IMAGE_H
