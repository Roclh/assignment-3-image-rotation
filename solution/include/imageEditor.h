#ifndef IMAGE_TRANSFORMER_IMAGEEDITOR_H
#define IMAGE_TRANSFORMER_IMAGEEDITOR_H
#include "BMP.h"
#include <stdbool.h>
#include <stdio.h>

int rotateBMPImage(char* source, char* dest);

#endif //IMAGE_TRANSFORMER_IMAGEEDITOR_H
