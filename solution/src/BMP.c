#include "BMP.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>

#define BM 19778;
#define OFFSET 54
#define INFO_HEADER_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define MAX_HEIGHT UINT32_MAX
#define MAX_WIDTH UINT32_MAX

#define FOR_BMP_HEADER(FOR_FIELD) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD(t, n) t n ;

struct __attribute__((packed)) bmp_header {
    FOR_BMP_HEADER(DECLARE_FIELD)
};

static bool checkType(struct bmp_header bmpHeader);

static bool getPixels(FILE *file, uint32_t offset, struct image img);

int fromBMP(FILE *to, struct image *img) {
    if (!to) {
        logErrorDescriptionWithLabel(READ_FILE_ERROR, "solution/src/BMP.c:44");
        return READ_FILE_ERROR;
    }
    if (!img) {
        logErrorDescriptionWithLabel(WRONG_POINTER_ERROR, "solution/src/BMP.c:48");
        return WRONG_POINTER_ERROR;
    }
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, to)) {
        logErrorDescriptionWithLabel(WRONG_BMP_HEADER_ERROR, "solution/src/BMP.c:53");
        return WRONG_BMP_HEADER_ERROR;
    }
    if (checkType(header)) {
        logErrorDescriptionWithLabel(WRONG_BMP_SIGNATURE_ERROR, "solution/src/BMP.c:57");
        return WRONG_BMP_SIGNATURE_ERROR;
    }
    const uint32_t height = header.biHeight;
    const uint32_t width = header.biWidth;
    const uint32_t offset = header.bOffBits;
    *img = initImage(width, height);
    if (!img->data) {
        logErrorDescriptionWithLabel(READ_NO_MEM, "solution/src/BMP.c:65");
        return READ_NO_MEM;
    }
    if (!getPixels(to, offset, *img)) {
        logErrorDescriptionWithLabel(READ_INVALID_BITS, "solution/src/BMP.c:69");
        return READ_INVALID_BITS;
    }
    return 0;
}

static bool buildHeader(uint32_t width, uint32_t height,
                        const struct pixel *data, struct bmp_header *header);

static bool writeHeader(FILE *file, struct bmp_header *header);

static bool writePixels(FILE *file, uint32_t offset, struct image img);

int toBMP(FILE *out, struct image img) {
    if (!out) {
        logErrorDescriptionWithLabel(WRITE_FILE_ERROR, "solution/src/BMP.c:84");
        return WRITE_FILE_ERROR;
    }
    const uint32_t height = img.height;
    const uint32_t width = img.width;
    if (img.height > MAX_HEIGHT || img.width > MAX_WIDTH || !img.data) {
        logErrorDescriptionWithLabel(INVALID_IMAGE_ERROR, "solution/src/BMP.c:90");
        return INVALID_IMAGE_ERROR;
    }
    struct pixel *data = img.data;
    struct bmp_header header = {0};
    if (!buildHeader(width, height, data, &header)) {
        logErrorDescriptionWithLabel(INVALID_IMAGE_ERROR, "solution/src/BMP.c:96");
        return INVALID_IMAGE_ERROR;
    }
    const uint32_t offset = header.bOffBits;
    if (!writeHeader(out, &header)) {
        return WRITE_FILE_ERROR;
    }
    if (!writePixels(out, offset, img)) {
        return WRITE_FILE_ERROR;
    }
    return 0;
}

static bool checkType(struct bmp_header bmpHeader) {
    return bmpHeader.bfType ^ (uint16_t) BM;
}

static uint8_t calculatePadding(uint32_t width) {
    return (4 - (sizeof(struct pixel) * width) % 4) % 4;
}

static int compareUint(uint32_t first, uint32_t second) {
    if (first > second) return 1;
    if (first < second) return -1;
    return 0;
}

static bool getPixels(FILE *file, uint32_t offset, struct image img) {
    const uint32_t width = img.width;
    const uint32_t height = img.height;
    const uint8_t padding = calculatePadding(width);
    if (fseek(file, offset, SEEK_SET) < 0) {
        logErrorDescriptionWithLabel(READ_FILE_ERROR, "solution/src/BMP.c:127");
        return false;
    }
    for (uint32_t i = height; i > 0; --i) {
        uint32_t read = fread(target(img, 0, i - 1), sizeof(struct pixel), width, file);
        if (compareUint(read, width) < 0) {
            printf("iteration: %i, read: %i, width: %i, eof: %i, errorcode: %i\n", i, read, width, feof(file), ferror(file));
            logErrorDescriptionWithLabel(READ_FILE_ERROR, "solution/src/BMP.c:132 invalid image result size");
            return false;
        }
        int check = fseek(file, padding, SEEK_CUR);
        if (check < 0) {
            printf("%i\n", check);
            logErrorDescriptionWithLabel(READ_FILE_ERROR, "solution/src/BMP.c:136");
            return false;
        }
    }
    return true;
}

static bool buildHeader(uint32_t width, uint32_t height,
                        const struct pixel *data, struct bmp_header *header) {
    if (!data || width == 0 || height == 0 || !header)
        return false;
    header->biWidth = width;
    header->biHeight = height;
    const uint32_t sizeWithoutPadding = sizeof(struct pixel) * width * height;
    const uint32_t sizeOfPadding = calculatePadding(width) * height;
    header->biSizeImage = sizeWithoutPadding + sizeOfPadding;
    header->bfileSize = sizeof(struct bmp_header) + header->biSizeImage;
    header->bfType = BM;
    header->biBitCount = BIT_COUNT;
    header->biCompression = COMPRESSION;
    header->biPlanes = PLANES;
    header->bOffBits = OFFSET;
    header->biSize = INFO_HEADER_SIZE;
    return true;
}

static bool writeHeader(FILE *file, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, file);
}

static bool writePixels(FILE *file, uint32_t offset, struct image img) {
    if (fseek(file, offset, SEEK_SET) < 0)
        return false;
    const uint32_t width = img.width;
    const uint32_t height = img.height;
    const uint8_t padding = calculatePadding(width);
    const uint8_t zero[3] = {0, 0, 0};
    for (uint32_t i = height; i > 0; --i) {
        if (fwrite(target(img, 0, i - 1), sizeof(struct pixel), width, file) < width) {
            logErrorDescriptionWithLabel(WRITE_FILE_ERROR, "solution/src/BMP.c:176");
            return false;
        }
        if (fwrite(&zero, 1, padding, file) < padding) {
            logErrorDescriptionWithLabel(WRITE_FILE_ERROR, "solution/src/BMP.c:180");
            return false;
        }
    }
    return true;
}






