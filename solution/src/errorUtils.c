#include "errorUtils.h"

void logErrorDescription(enum error err){
    switch (err) {
        case OPEN_FILE_ERROR:
            printf("Error with code %d: Failed to open file\n", err);
            break;
        case WRITE_FILE_ERROR:
            printf("Error with code %d: Failed to write in file\n", err);
            break;
        case READ_FILE_ERROR:
            printf("Error with code %d: Failed to read file\n", err);
            break;
        case CLOSE_FILE_ERROR:
            printf("Error with code %d: Failed to close file\n", err);
            break;
        case READ_NO_MEM:
            printf("Error with code %d: READ NO MEM\n", err);
            break;
        case READ_INVALID_BITS:
            printf("Error with code %d: READ INVALID BITS\n", err);
            break;
        case WRONG_ARG_ERROR:
            printf("Error with code %d: Wrong argument error\n", err);
            break;
        case IMAGE_ROTATION_ERROR:
            printf("Error with code %d: Image rotation error\n", err);
            break;
        case WRONG_POINTER_ERROR:
            printf("Error with code %d: Wrong pointer error\n", err);
            break;
        case WRONG_BMP_HEADER_ERROR:
            printf("Error with code %d: Wrong BMP header error\n", err);
            break;
        case WRONG_BMP_SIGNATURE_ERROR:
            printf("Error with code %d: Wrong BMP signature error\n", err);
            break;
        case INVALID_IMAGE_ERROR:
            printf("Error with code %d: Invalid image error\n", err);
            break;
    }
}

void logErrorDescriptionWithLabel(enum error err, char* label){
    logErrorDescription(err);
    printf("%s\n", label);
}
