#include "fileUtils.h"


int readF(const char *pathname, FILE **file) {
    if (!pathname) {
        logErrorDescriptionWithLabel(OPEN_FILE_ERROR, "fileUtils.c:6");
        return OPEN_FILE_ERROR;
    }
    FILE *f = fopen(pathname, "rb");
    if (f) {
        *file = f;
        return 0;
    }
    logErrorDescriptionWithLabel(READ_FILE_ERROR, "fileUtils.c:14");
    return READ_FILE_ERROR;
}

int writeF(const char *pathname, FILE **file) {
    if (!pathname) {
        logErrorDescriptionWithLabel(OPEN_FILE_ERROR, "fileUtils.c:20");
        return OPEN_FILE_ERROR;
    }
    FILE *f = fopen(pathname, "wb");
    if (f) {
        *file = f;
        return 0;
    }
    logErrorDescriptionWithLabel(WRITE_FILE_ERROR, "fileUtils.c:28");
    return WRITE_FILE_ERROR;
}

int closeF(FILE *file) {
    if (!file) {
        logErrorDescriptionWithLabel(CLOSE_FILE_ERROR, "fileUtils.c:34");
        return CLOSE_FILE_ERROR;
    }
    int status = fclose(file);
    if (status != 0) {
        logErrorDescriptionWithLabel(CLOSE_FILE_ERROR, "fileUtils.c:39");
        return CLOSE_FILE_ERROR;
    }
    return status;
}
