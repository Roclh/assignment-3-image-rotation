#include "image.h"
#include "malloc.h"
#include "stdint.h"

const struct image wrong_image = {
        .width = 0,
        .height = 0,
        .data = NULL
};

struct image initImage(uint64_t width, uint64_t height){
    struct image img = {0};
    img.width = width;
    img.height = height;
    img.data = malloc(sizeof(struct pixel)*width*height);
    if(!img.data) {
        return wrong_image;
    }
    return img;
}

bool destroyImage(struct image *image){
    if(image -> data){
        free(image -> data);
    }
    image->height = 0;
    image->width = 0;
    image->data = NULL;
    return true;
}

struct pixel* target(struct image img, uint64_t x, uint64_t y){
    if(img.data){
        return &img.data[x + y * img.width];
    }else{
        return NULL;
    }
}
