#include "imageEditor.h"

int rotateImage(struct image src, struct image *out);

int rotateBMPImage(char *source, char *dest) {
    FILE *input = {0};
    if (readF(source, &input))
        return 1;
    struct image image;
    if (fromBMP(input, &image)){
        destroyImage(&image);
        return 1;
    }
    if (closeF(input)){
        destroyImage(&image);
        return 1;
    }
    FILE *output = {0};
    if (writeF(dest, &output)){
        destroyImage(&image);
        return 1;
    }
    struct image newImage;
    if (rotateImage(image, &newImage)){
        destroyImage(&image);
        destroyImage(&newImage);
        return 1;
    }
    if (toBMP(output, newImage)){
        destroyImage(&image);
        destroyImage(&newImage);
        return 1;
    }
    if (closeF(output)){
        destroyImage(&image);
        destroyImage(&newImage);
        return 1;
    }
    if (destroyImage(&image)){
        destroyImage(&newImage);
        return 1;
    }
    if (destroyImage(&newImage))
        return 1;
    return 0;
}

int rotateImage(struct image src, struct image *out) {
    struct image img = initImage(src.height, src.width);
    if (!img.data) {
        logErrorDescriptionWithLabel(IMAGE_ROTATION_ERROR, "imageEditor.c:49");
        return 1;
    }
    for (uint64_t y = 0; y < src.height; y++) {
        for (uint64_t x = 0; x < src.width; x++) {
            struct pixel *pixel = target(src, x, y);
            uint64_t newX = y;
            uint64_t newY = img.height - x - 1;
            *target(img, newX, newY) = *pixel;
        }
    }
    *out = img;
    return 0;
}

