#include "errorUtils.h"
#include "imageEditor.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if(argc != 3){
        printf("usage ./%s <source-image> <transformed-image>", argv[0]);
        logErrorDescriptionWithLabel(WRONG_ARG_ERROR, "solution/src/main.c:8");
        return WRONG_ARG_ERROR;
    }
    if(!rotateBMPImage(argv[1], argv[2])){
        logErrorDescriptionWithLabel(IMAGE_ROTATION_ERROR, "solution/src/main.c:13");
        return IMAGE_ROTATION_ERROR;
    }
    return 0;
}
